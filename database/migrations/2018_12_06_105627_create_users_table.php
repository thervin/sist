<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('firstname', 100)->nullable();
			$table->string('lastname', 100)->nullable();
			$table->string('email', 100)->unique();
			$table->string('password', 100)->nullable();
			$table->string('salt', 30)->nullable();
			$table->string('register_ip', 15)->nullable();
			$table->string('forget_token', 100)->nullable();
			$table->string('active_token', 100)->nullable();
            $table->timestamp('email_verified_at')->nullable();
			$table->boolean('active')->nullable();
			$table->string('activation_key', 100)->nullable();
            $table->rememberToken();
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}